#!/usr/bin/env python3

import enum
import time
from pyftdi import i2c


class Lcd():
    # {{{ Pins hooked up in in 4-bit mode
    # MSB                LSB
    # D7 D6 D5 D4 K E RW RS
    #  |  |  |  | | |  |  +- Register select, 0: command, 1: data
    #  |  |  |  | | |  +---- Read/Write, 0: write, 1: read
    #  |  |  |  | | +------- Clock(Enable), Falling edge triggered
    #  |  |  |  | +--------- Backlight Cathode via FET, 0: off, 1: on
    #  |  |  |  +----------- Bit 4
    #  |  |  +-------------- Bit 5
    #  |  +----------------- Bit 6
    #  +-------------------- Bit 7
    # }}}
    Pin = enum.IntFlag("Pin", "RS RW E K D4 D5 D6 D7")  # LSB - MSB

    def __init__(self, pid, vid, port, init=True):
        self.port = self._get_port(pid, vid, port)
        self._buffer = bytearray()
        if init:
            self.init()

    def __enter__(self):
        self.init()
        self.enable()
        return self

    def __exit__(self, *args):
        self.flush()
        self.terminate()

    def _get_port(self, pid, vid, port):
        self.bus = i2c.I2cController()
        self.bus.configure(f"ftdi://{pid}:{vid}/1")
        return self.bus.get_port(port)

    def init(self):
        # put display in known state
        self._write_nibble(self.Pin.D5 | self.Pin.D4)
        time.sleep(0.005)  # datasheet requires >= 4.1ms sleep
        self._write_nibble(self.Pin.D5 | self.Pin.D4)
        self._write_nibble(self.Pin.D5 | self.Pin.D4)
        # 4-bit mode
        self._write_nibble(self.Pin.D5)
        # 2 line 5x8 font
        self._write_nibble(self.Pin.D5)
        self._write_nibble(self.Pin.D7)
        # display off
        self._write_nibble(0x00)
        self._write_nibble(self.Pin.D7)
        # display clear
        self.clear()
        # entry mode, increment cursor, do not shift
        self._write_nibble(0x00)
        self._write_nibble(self.Pin.D6 | self.Pin.D5)

    def _write_nibble(self, nibble, backlight=True):
        if backlight:
            nibble = nibble | self.Pin.K
        nibble = nibble | self.Pin.E
        self.port.write(bytes([nibble]))
        nibble = nibble ^ self.Pin.E
        self.port.write(bytes([nibble]))

    def _pulse_out(self, nibble):
        nibble = nibble | self.Pin.E
        self._buffer.append(nibble)
        nibble = nibble ^ self.Pin.E
        self._buffer.append(nibble)

    def write(self, data, backlight=True, register_select=False, read_write=False, flush=True):
        if register_select:
            register_select = self.Pin.RS
        if read_write:
            read_write = self.Pin.RW
        if backlight:
            backlight = self.Pin.K

        for byte in data:
            # shift left 4 to map to the 4 data pins
            high = (byte >> 4 << 4) | register_select | read_write | backlight
            low = ((byte & 0x0F) << 4) | register_select | read_write | backlight
            self._pulse_out(high)
            self._pulse_out(low)
        if flush:
            self.flush()

    def flush(self):
        self.port.write(self._buffer)
        self._buffer = bytearray()

    def enable(self):
        self.write(b"\x0C")

    def clear(self):
        self.write(b"\x01")

    def terminate(self):
        self.bus.terminate()
