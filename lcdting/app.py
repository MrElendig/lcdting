#!/usr/bin/env python3

import time
from pathlib import Path
from contextlib import ExitStack
from lcdting.lcd import Lcd


HWMON_PATH = Path("/sys/class/hwmon/")
SENSORS = [f"temp{n}_input" for n in range(1, 6)]

def hwmon_path(basepath):
    for path in (x for x in basepath.iterdir() if x.is_dir()):
        name = path / "name"
        if name.read_text().strip() == "coretemp":
            return path

def main():
    base_path = hwmon_path(HWMON_PATH)
    with Lcd("ftdi", "232h", 0x27) as lcd:
        lcd.write(b"CPU Temperature", register_select=True)
        lcd.write(bytes([lcd.Pin.D7 | 0x40]))  # Second line
        lcd.write(b"PK C1 C2 C3 C4", register_select=True)
        with ExitStack() as stack:
            files = [stack.enter_context(open(base_path / fname, "rb")) for fname in SENSORS]
            out_data = [0]*5
            while True:
                for idx, file in enumerate(files):
                    file.flush()
                    file.seek(0)
                    temp = file.readline()[:2] # values in °C, zero padded on the right
                    out_data[idx] = temp
                lcd.write(bytes([lcd.Pin.D7 | 0x14])) # cursor to start of third line
                lcd.write(b"%s %s %s %s %s" % tuple(out_data), register_select=True)
                time.sleep(1)


if __name__ == "__main__":
    main()
