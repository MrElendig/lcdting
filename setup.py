import setuptools


setuptools.setup(
    name="lcdting",
    version="0.0.1",
    author="Øyvind Heggstad",
    author_email="heggstad@gmail.com",
    description="A silly little tool to poke at a 4x40 character lcd",
    packages=setuptools.find_packages(),
    classifiers=(
        "Programming Language :: Python :: 3",
        " Filter License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
        "Operating System :: OS Independent",
    ),
    entry_points={"console_scripts":[
        "lcdting = lcdting.app:main"
    ]}
)
